<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>X6 Feel</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap-4.2.1.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet" type="text/css">
</head>
<body>

<?php
    require_once("includes/php_tsf.php");
    classifyTweets();
?>

<!-- body code goes here -->
<div class="container-fluid">
    <div>
        <a href="index.php">
            <img src="img/Asset_1.png" width="150" height="150" alt=""/>
        </a>
    </div>
    <div class="row justify-content-center">
        <h1 style="color: white"><?php echo getSearch(); ?></h1>
    </div>
    <div class="row justify-content-center">
        <form name="Search bar" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="get" aria-label="Search bar">
            <div class="input-group input-group-lg">
                <div class="input-group-prepend">
                    <div id="searchTypeIndicator" class="input-group-text">
                    <?php
                    if (getSearch()[0] != "@")
                      echo "#";
                    else
                      echo "@";
                    ?>
                    </div>
                </div>
                <!--<select class="input-group-prepend" id="sel1">
                  <option>#</option>
                  <option>@</option>
                </select>-->
                <input type="text" class="form-control" name="query" placeholder="Search...">
                <div class="input-group-append">
                    <input class="btn btn-primary" type="submit" value="Search"/>
                </div>
            </div>
            <label style="color: white">Search by:</label>
            <label id="hashtagRadioButton" class="radio-inline" style="color: white"><input type="radio" name="type" value="#" <?php if (getSearch()[0] != "@") echo "checked"; ?>>Hashtag</label>
            <label id="usernameRadioButton" class="radio-inline" style="color: white"><input type="radio" name="type" value="@" <?php if (getSearch()[0] == "@") echo "checked"; ?>>Person</label>
        </form>
    </div>
    <div class="row result justify-content-center">
		    <figure id="graph-holder" alt="Pie chart showing the percentage of positive, negative and neutral tweets"></figure>
    </div>
    <div class="row result">
        <p>We searched <?php echo getTweetNo(); ?> tweets, here are some highlights:</p>
    </div>
    <div class="row result">
        <section class="col-md" aria-labelledby="positiveHeading">
				    <h2 id="positiveHeading" class="positive">
                <?php
                echo getPositiveNo();
                echo '% positive'
                ?>
            </h2>
			    
            <div id="positiveTweetCarousel" class="carousel slide justify-content-center" data-interval="false">
              <?php echo getPositiveTweets(5); ?>
            </div>
        </section>
        <section class="col-md" aria-labelledby="neutralHeading">
            <h2 id="neutralHeading" class="neutral">
                <?php
                echo getNeutralNo();
                echo '% neutral'
                ?>
            </h2>
            <div id="neutralTweetCarousel" class="carousel slide justify-content-center" data-interval="false">
              <?php echo getNeutralTweets(5); ?>
            </div>
        </section>
        <section class="col-md" aria-labelledby="negativeHeading">
            <h2 id="negativeHeading" class="negative">
                <?php
                echo getNegativeNo();
                echo '% negative'
                ?>
            </h2>
            <div id="negativeTweetCarousel" class="carousel slide justify-content-center" data-interval="false">
              <?php echo getNegativeTweets(5); ?>
            </div>
        </section>
    </div>
    <!-- body code goes here -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.3.1.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap-4.2.1.js"></script>

    <script src="js/p5.min.js"></script>
    <script src="js/graph.js"></script>
    <script src="js/<?php showResults(); ?>Results.js"></script>
    <script sync src="https://platform.twitter.com/widgets.js"></script>
    <script src="js/embedTweets.js"></script>
    <script src="js/radioButtonListener.js"></script>
    
    <script>
      percentage = [<?php echo getPositiveNo(); ?>, <?php echo getNeutralNo(); ?>, <?php echo getNegativeNo(); ?>];
      colors = ['#AFD275', '#45A29E', '#E7717D']

      function setup() {
          var canvas = createCanvas(300, 300);
          canvas.parent('graph-holder');
          noStroke();
          angleMode(DEGREES);
          noLoop(); // Run once and stop
      }

      function draw() {
          pieChart(250, percentage);
      }

      function pieChart(diameter, data) {
          let lastAngle = 0;
          for (let i = 0; i < data.length; i++) {
              fill(colors[i]);
              arc(
                  width / 2,
                  height / 2,
                  diameter,
                  diameter,
                  lastAngle,
                  lastAngle + (360 * percentage[i] / (percentage[0] + percentage[1] + percentage[2]))
              );
              lastAngle += (360 * percentage[i] / (percentage[0] + percentage[1] + percentage[2]));
          }
      }
    </script>
</body>
</html>
