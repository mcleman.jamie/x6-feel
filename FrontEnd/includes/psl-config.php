<?php
/**
 * Created by PhpStorm.
 * User: Albert Viilik
 * Date: 22/02/2019
 * Time: 20:21
 */

/**
 * These are the database login details
 */
define("HOST", "dbhost.cs.man.ac.uk");     // The host you want to connect to.
define("USER", "t23437nm");    // The database username.
define("PASSWORD", "SQLPassword");    // The database password.
define("DATABASE", "2018_comp10120_x6");    // The database name.

define("CAN_REGISTER", "any");
define("DEFAULT_ROLE", "member");

define("SECURE", FALSE);    // FOR DEVELOPMENT ONLY!!!!
?>
