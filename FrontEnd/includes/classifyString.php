﻿<?php
function classifyString($string) {

  // echo $string;

  //declaring the variables. Three variables required, the servername, username,
  //and password
  $serverName = "dbhost.cs.man.ac.uk";
  $userName = "q28836sg";
  $password = "ninetails";
  $group_dbnames = "2018_comp10120_x6";
  $conn = new mysqli ($serverName, $userName, $password, $group_dbnames);
  //check if the connection was successful or not
  if ($conn ->connect_error)
  {
    die ("Connection Error: " .$conn->connect_error);
  }

  ///////////////////////////////////////////////////////////////////////////////////

  //This .php file will contain the source code for the multiple functions that
  //need to be used in the main
  //<Insert list of functions over here>
  //<explain what each function does>
  //The first fucntion takes a string, and uses the explode method to break the
  //string into smaller sub strings and stores them into the array. The delimeter
  //will be the space character.

  //------------------------------------------------------------------------------

  //Convert string to lowercase and remove any characters not in [a-z] or a space character
  $string = strtolower($string);
  $newString = "";
  $stringArray = str_split($string);
  foreach ($stringArray as $char)
  {
    if ((97 <= ord($char) and ord($char) <= 122) or ord($char) == 32)
    {
      $newString = $newString . $char;
    }
  }

  $string = $newString;

  // now that we have striped punctuation marks, we have to strip the stopwords
  //<insert method that removes stopwords>

  $arrOfStrings = explode(" ", $string);
  $numOfwordsInString = count($arrOfStrings);
  $arrOfStopWords= array("a", "about", "above", "after", "again", "against",
  "all", "am", "an", "and", "any", "are", "as", "at", "be", "because", "been",
  "before", "being", "below", "between", "both", "but", "by", "could", "did",
  "do", "does", "doing", "down", "during", "each", "few", "for", "from",
  "further", "had", "has", "have", "having", "he", "he'd", "he'll", "he's",
  "her", "here", "here's", "hers", "herself", "him", "himself", "his", "how",
  "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "it",
  "it's", "its", "itself", "let's", "me", "more", "most", "my", "myself",
  "nor", "of", "on", "once", "only", "or", "other", "ought", "our", "ours",
  "ourselves", "out", "over", "own", "same", "she", "she'd", "she'll", "she's",
  "should", "so", "some", "such", "than", "that", "that's", "the", "their",
  "theirs", "them", "themselves", "then", "there", "there's", "these", "they",
  "they'd", "they'll", "they're", "they've", "this", "those", "through", "to",
  "too", "under", "until", "up", "very", "was", "we", "we'd", "we'll", "we're",
  "we've", "were", "what", "what's", "when", "when's", "where", "where's",
  "which", "while", "who", "who's", "whom", "why", "why's", "with", "would",
  "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself",
  "yourselves" );
  $string = str_replace($arrOfStopWords, "", $string);
  // now we have an array of substrings
  //------------------------------------------------------------------------------
  // now we have to grab the values from the sql data base.
  // so now we connect to the database
  //

  $sumOfLogProbabilityPositive = 0;
  $sumOfLogProbabilityNegative = 0;
  $sumOfLogProbabilityNeutral = 0;

  for($loopCount = 0; $loopCount < $numOfwordsInString; $loopCount++)
  {
    $sqlStatement  =  "SELECT Positive, Negative, Neutral FROM NaiveBayesClassifierModel WHERE Word = '$arrOfStrings[$loopCount]'";
    $result = mysqli_query($conn, $sqlStatement);
    if ($result->num_rows>0)
    {
      $row = $result -> fetch_assoc();
      $positiveProbability = $row["Positive"];
      $negativeProbability = $row["Negative"];
      $neutralProbability = $row["Neutral"];
      $sumOfLogProbabilityPositive += $positiveProbability;
      $sumOfLogProbabilityNegative += $negativeProbability;
      $sumOfLogProbabilityNeutral += $neutralProbability;
    }
  }

  $sqlStatementForUnknown = "SELECT Class, ClassProbability FROM NaiveBayesModelPhraseCount";
  $result = mysqli_query($conn, $sqlStatementForUnknown);
  if ($result->num_rows > 0)
  {
    // output data of each row
    while($row = $result->fetch_assoc())
    {
      switch ($row["Class"])
      {
        case 0:
          $positiveClassProbability = $row["ClassProbability"];
          break;
        case 1:
          $negativeClassProbability = $row["ClassProbability"];
          break;
        case 2:
          $neutralClassProbability = $row["ClassProbability"];
          break;
      }
    }
  }

  $sumOfLogProbabilityPositive += $positiveClassProbability;
  $sumOfLogProbabilityNegative += $negativeClassProbability;
  $sumOfLogProbabilityNeutral += $neutralClassProbability;

  $arrOfResults = array($sumOfLogProbabilityPositive, $sumOfLogProbabilityNegative, $sumOfLogProbabilityNeutral);

  return getMaxIndex($arrOfResults);
}

function getMaxIndex($array){
  $maxValueIndex = 0;
  for ($index = 0; $index < sizeOf($array); $index++) {
    if ($array[$index] > $array[$maxValueIndex])
      $maxValueIndex = $index;
  }
  return $maxValueIndex;
}

?>
