<?php

// Initialise global variables
$positive_count = $negative_count = $neutral_count = $total_count = 0;
$positive_tweets = $negative_tweets = $neutral_tweets = [];
$search_executed = False;
$search_query = "";
$search_symbol = "#";

function classifyTweets()
{
    require_once("search_by_hashtag_30day.php");
    require_once("search_by_hashtag_full.php");
    require_once("search_by_username_30day.php");
    require_once("search_by_username_full.php");
    require_once("search_by_whatever.php");
    require_once("includes/classifyString.php");
    $positive_tweets = $negative_tweets = $neutral_tweets = [];
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        // Check query not empty
        if (!empty($_GET["query"]))
        {
            // Gather tweets using the Twitter API.
            // If type="@", it searches by Twitter user
            // Otherwise it searches by hashtag as the default behaviour
            $query = $_GET["query"];
            if (!empty($_GET["type"]) && $_GET["type"] == "@")
            {
              $GLOBALS['search_symbol'] = "@";
              $tweets = search_by_username_30day($query);
            }
            else
            {
              $GLOBALS['search_symbol'] = "#";
              $tweets = search_by_hashtag_30day($query);
            }
            
            // Classify each tweet and store in the respective arrays for each class
            $positive_count = $negative_count = $neutral_count = 0;
            for ($index = 0; $index < sizeOf($tweets[0]); $index++) {
                $tweet = $tweets[1][$index];
                $tweet_id = $tweets[0][$index];
                if ($tweet != "") {
                    $class = classifyString($tweet);
                    switch ($class) {
                        case 0:
                            $positive_count += 1;
                            array_push($positive_tweets, $tweet_id);
                            break;
                        case 1:
                            $negative_count += 1;
                            array_push($negative_tweets, $tweet_id);
                            break;
                        case 2:
                            $neutral_count += 1;
                            array_push($neutral_tweets, $tweet_id);
                            break;
                    }
                }
            }
            
            // Update global variables
            $GLOBALS['search_query'] = $query;
            $GLOBALS['negative_count'] = $negative_count;
            $GLOBALS['neutral_count'] = $neutral_count;
            $GLOBALS['positive_count'] = $positive_count;
            $GLOBALS['total_count'] = $negative_count + $neutral_count + $positive_count;
            $GLOBALS['negative_tweets'] = $negative_tweets;
            $GLOBALS['neutral_tweets'] = $neutral_tweets;
            $GLOBALS['positive_tweets'] = $positive_tweets;
            $GLOBALS['search_executed'] = True;
        }
    }
}

// Return the users search term
function getSearch()
{
    if ($GLOBALS['search_executed']) {
        return $GLOBALS['search_symbol'] . $GLOBALS['search_query'];
    }
}

// Get percentage of positive tweets
function getPositiveNo()
{
    return round(100 * $GLOBALS['positive_count'] / $GLOBALS['total_count'], 1);
}

// Get percentage of neutral tweets
function getNeutralNo()
{
    return round(100 * $GLOBALS['neutral_count'] / $GLOBALS['total_count'], 1);
}

// Get percentage of negative tweets
function getNegativeNo()
{
    return round(100 * $GLOBALS['negative_count'] / $GLOBALS['total_count'], 1);
}

function getTweetNo()
{
    return $GLOBALS['total_count'];
}

// Returns carousel of positive tweets
// The number of tweets displayed is specified by $tweetCount or limited by the
// number of positive tweets
function getPositiveTweets($tweetCount)
{
    if ($GLOBALS['search_executed']) {
        if (sizeOf($GLOBALS['positive_tweets']) > 0)
        {
            $result = '<ol class="carousel-indicators">
                       <li data-target="#positiveTweetCarousel" data-slide-to="0" class="active"></li>';
            for ($index = 1; $index < $tweetCount && $index < sizeOf($GLOBALS['positive_tweets']); $index++) {
                $result .= '<li data-target="#positiveTweetCarousel" data-slide-to="' . $index . '"></li>';
            }
            $result .= '</ol>
                        <div class="carousel-inner">
                        <div class="carousel-item active">
                        <div class="tweet" tweetID=' . $GLOBALS['positive_tweets'][0] . '></div>
                        </div>';
            for ($index = 1; $index < $tweetCount && $index < sizeOf($GLOBALS['positive_tweets']); $index++) {
                $result .= '<div class="carousel-item">
                            <div class="tweet" tweetID=' . $GLOBALS['positive_tweets'][$index] . '></div>
                            </div>';
            }
            $result .= '</div>
                        <a class="carousel-control-prev" href="#positiveTweetCarousel" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#positiveTweetCarousel" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>';
            return $result;
        }
    }
}

// Returns carousel of neutral tweets
// The number of tweets displayed is specified by $tweetCount or limited by the
// number of neutral tweets
function getNeutralTweets($tweetCount)
{
    if ($GLOBALS['search_executed']) {
        if (sizeOf($GLOBALS['neutral_tweets']) > 0)
        {
            $result = '<ol class="carousel-indicators">
                       <li data-target="#neutralTweetCarousel" data-slide-to="0" class="active"></li>';
            for ($index = 1; $index < $tweetCount && $index < sizeOf($GLOBALS['neutral_tweets']); $index++) {
                $result .= '<li data-target="#neutralTweetCarousel" data-slide-to="' . $index . '"></li>';
            }
            $result .= '</ol>
                        <div class="carousel-inner">
                        <div class="carousel-item active">
                        <div class="tweet" tweetID=' . $GLOBALS['neutral_tweets'][0] . '></div>
                        </div>';
            for ($index = 1; $index < $tweetCount && $index < sizeOf($GLOBALS['neutral_tweets']); $index++) {
                $result .= '<div class="carousel-item">
                            <div class="tweet" tweetID=' . $GLOBALS['neutral_tweets'][$index] . '></div>
                            </div>';
            }
            $result .= '</div>
                        <a class="carousel-control-prev" href="#neutralTweetCarousel" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#neutralTweetCarousel" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>';
            return $result;
        }
    }
}

// Returns carousel of negative tweets
// The number of tweets displayed is specified by $tweetCount or limited by the
// number of negative tweets
function getNegativeTweets($tweetCount)
{
    if ($GLOBALS['search_executed']) {
        if (sizeOf($GLOBALS['negative_tweets']) > 0)
        {
            $result = '<ol class="carousel-indicators">
                       <li data-target="#negativeTweetCarousel" data-slide-to="0" class="active"></li>';
            for ($index = 1; $index < $tweetCount && $index < sizeOf($GLOBALS['negative_tweets']); $index++) {
                $result .= '<li data-target="#negativeTweetCarousel" data-slide-to="' . $index . '"></li>';
            }
            $result .= '</ol>';
            $result .= '<div class="carousel-inner">';
            $result .= '<div class="carousel-item active">';
            $result .= '<div class="tweet" tweetID=' . $GLOBALS['negative_tweets'][0] . '></div>';
            $result .= '</div>';
            for ($index = 1; $index < $tweetCount && $index < sizeOf($GLOBALS['negative_tweets']); $index++) {
                $result .= '<div class="carousel-item">';
                $result .= '<div class="tweet" tweetID=' . $GLOBALS['negative_tweets'][$index] . '></div>';
                $result .= '</div>';
            }
            $result .= '</div>
                        <a class="carousel-control-prev" href="#negativeTweetCarousel" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#negativeTweetCarousel" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>';
            return $result;
        }
    }
}

// Function to control if result summaries are displayed
function showResults()
{
    if ($GLOBALS['search_executed']) {
        echo "show";
    }
    else {
      echo "hide";
    }
}


?>
