<?php

include_once 'includes/db_connect.php';
include_once 'includes/psl-config.php';
include_once 'includes/functions.php';
sec_session_start();
// not log in
if(!isset($_SESSION['username'])){
    header("Location: index.php");
}
$key = '';
if(isset($_GET['key'])) {
    $key=$_GET['key'];
}
$sql0 = "SELECT count(id) as num FROM `news` WHERE `text` like '%$key%' and pos=0";
$pos0 = $mysqli->query($sql0)->fetch_assoc()['num'];


$sql1 = "SELECT count(id) as num FROM `news` WHERE `text` like '%$key%' and pos=1";
$pos1 = $mysqli->query($sql1)->fetch_assoc()['num'];
$sql2 = "SELECT count(id) as num FROM `news` WHERE `text` like '%$key%' and pos=1";
$pos2 = $mysqli->query($sql2)->fetch_assoc()['num'];

// record history
if(isset($_GET['key']) && strlen($_GET['key'])>0) {
    $user_id = $_SESSION['user_id'];
    $username = $_SESSION['username'];
    $key=$_GET['key'];
    $mysqli->query("INSERT INTO history(user_id, username, text)
                                    VALUES ('$user_id', '$username','$key')");
}

// search history
$sql = "select * from history order by time desc limit 10;";
$history=$mysqli->query($sql)->fetch_all(MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Search Page</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap-4.2.1.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet" type="text/css">
</head>
<body>

<div style="position:relative;">

    <!--    导航栏   -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" style="">
        <a class="navbar-brand" href="protected_page.php">
            <img src="img/Asset 2.png" alt="" width=40 height=40 style="border: 1px solid green;"> <!-- replace logo here -->
            Logo
        </a>
        <ul class="navbar-nav" style="    position: absolute;
    right: 100px;">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $_SESSION['username'] ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="includes/logout.php">Logout</a>
                </div>
            </li>
        </ul>
    </nav>

    <!-- body code goes here -->
    <div class="container-fluid">
        <div class="container" style="margin-top: 120px;overflow: scroll;">
            <div class="row">
                <div class="col"></div>
                <form action="protected_page.php" class="col" method="get">
                    <div class="input-group mb-3 input-group-lg">
                        <input type="text" class="form-control"
                               value="<?php if(isset($_GET['key'])) echo $_GET['key']; ?>"
                               name="key" placeholder="Input filter text...">
                        <div class="input-group-append ">
                            <button class="btn btn-primary" type="submit">Search</button>
                        </div>
                    </div>
                </form>
                <div class="col"></div>
            </div>
            <div style="margin: 10px auto;width: 600px;
            <?php if(isset($_GET['key'])) echo "display: block;";
                 else echo "display: none;";?>">
                <div id="text-holder" class="show_wrapper" style="width: 600px;">
                    <div style="display: inline-block;padding: 10px;">
                        <p style="margin-top: 60%">Positive Context: <b><?php echo $pos1; ?></b></p>
                        <p>Negative Context: <b><?php echo $pos2; ?></b></p>
                        <p>Neutral Context: <b><?php echo $pos0; ?></b></p>
                    </div>
                    <div id="holder" style="display: inline-block;">
                    </div>
                </div>
            </div>

            <div class="show_wrapper" style="margin: 10px auto;width: 400px;padding: 10px;">
                <h4><center>Top 10 recent Search History</center></h4>
                <table class="table table-bordered table-dark" >
                    <thead>
                    <tr><th>User</th><th>Text</th><th>Time</th></tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($history as $item):
                        ?>
                        <tr>
                            <td><?php echo $item['username'] ?></td>
                            <td><?php echo $item['text'] ?></td>
                            <td><?php echo $item['time'] ?></td>
                        </tr>

                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- body code goes here -->

    <!--    Background Click trigger-->
    <div style="opacity: 0;">
        <div class="north_america" style="width: 500px;height: 450px;border: 1px solid red;
            position: absolute;left: 180px;top: 70px;"
             data-toggle="modal" data-target="#north_america"></div>

        <div class="south_america" style="width: 340px;height: 400px;border: 1px solid red;
            position: absolute;left: 500px;top: 600px;"
             data-toggle="modal" data-target="#south_america"></div>

        <div class="europe" style="width: 200px;height: 200px;border: 1px solid red;
            position: absolute;left: 1000px;top: 100px;"
             data-toggle="modal" data-target="#europe"></div>

        <div class="africa" style="width: 350px;height: 400px;border: 1px solid red;
            position: absolute;left: 850px;top: 320px;"
             data-toggle="modal" data-target="#africa"></div>
        <div class="asia" style="width: 260px;height: 400px;border: 1px solid red;
            position: absolute;left: 1200px;top: 30px;"
             data-toggle="modal" data-target="#asia"></div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="north_america" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">North America</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Geovanis helped organize a 1996 trip to Moscow by Trump, who was in the early stages of pursuing what would become a long-held goal of building a Trump Tower in the Russian capital, according to multiple media reports at the time.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="south_america" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">South America</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                South America is a continent in the Western Hemisphere, mostly in the Southern Hemisphere, with a relatively small portion in the Northern Hemisphere. It may also be considered a subcontinent of the Americas,[5][6] which is how it is viewed in the Spanish and Portuguese-speaking regions of the Americas.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="europe" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Europe</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Europe is a continent located entirely in the Northern Hemisphere and mostly in the Eastern Hemisphere. It is bordered by the Arctic Ocean to the north, the Atlantic Ocean to the west and the Mediterranean Sea to the south. It comprises the westernmost part of Eurasia.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="africa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Africa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Africa is the world's second largest and second most-populous continent, being behind Asia in both categories. At about 30.3 million km2 (11.7 million square miles) including adjacent islands, it covers 6% of Earth's total surface area and 20% of its land area.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="asia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Asia (/ˈeɪʒə, ˈeɪʃə/ (About this soundlisten)) is Earth's largest and most populous continent, located primarily in the Eastern and Northern Hemispheres. It shares the continental landmass of Eurasia with the continent of Europe and the continental landmass of Afro-Eurasia with both Europe and Africa. Asia covers an area of 44,579,000 square kilometres (17,212,000 sq mi), about 30% of Earth's total land area and 8.7% of the Earth's total surface area.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-3.3.1.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/popper.min.js"></script>
<script src="js/bootstrap-4.2.1.js"></script>

<script src="js/d3.js"></script>
<script type="text/javascript">
    var width = 400;      //设置svg区域的宽度
    var height = 400;     //设置svg区域的高度

    var svg = d3.select('#holder')               //选择body区
        .append('svg')                //在body中添加svg
        .attr('width', width)					//将宽度赋给width属性
        .attr('height', height);      //将高度赋给height属性

    //确定初始数据
    var pos1 = ['Positive', <?php echo $pos1; ?>];
    var pos2 = ['Negative', <?php echo $pos2; ?>];
    var pos0 = ['Neutral', <?php echo $pos0; ?>];
    var dataset = [];
    if(pos1[1]>0) dataset.push(pos1);
    if(pos2[1]>0) dataset.push(pos2);
    if(pos0[1]>0) dataset.push(pos0);

    //转换数据
    var pie = d3.layout.pie()
        .value(function (d) { return d[1]; });
    var piedata = pie(dataset);
    console.log(piedata);

    //外半径和内半径
    var outerRadius = width / 3;
    var innerRadius = 0;

    //创建弧生成器
    var arc = d3.svg.arc()
        .innerRadius(innerRadius)
        .outerRadius(outerRadius);
    var color = d3.scale.category20();

    //添加对应数目的弧组，即<g>元素
    var arcs = svg.selectAll('g')
        .data(piedata)
        .enter()
        .append('g')
        .attr('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')');

    //添加弧的路径元素
    arcs.append('path')
        .attr('fill', function(d,i) {
            return color(i);         //设定弧的颜色
        })
        .attr('d', function(d) {
            return arc(d);           //使用弧生成器
        });


    //添加弧内的文字元素
    arcs.append('text')
        .attr('transform', function(d) {
            var x = arc.centroid(d)[0] * 1.4;      //文字的x坐标
            var y = arc.centroid(d)[1] * 1.4;      //文字的y坐标
            return 'translate(' + x + ',' + y + ')';
        })
        .attr('text-anchor', 'middle')
        .text(function(d) {
            //计算市场份额和百分比
            var percent = Number(d.value) / d3.sum(dataset, function(d) { return d[1]; }) * 100;
            //保留一个小数点，末尾加一个百分号返回
            return percent.toFixed(1) + '%';
        });

    //添加连接弧外的直线元素
    arcs.append('line')
        .attr('stroke', 'black')
        .attr('x1', function(d) { return arc.centroid(d)[0] * 2; })
        .attr('y1', function(d) { return arc.centroid(d)[1] * 2; })
        .attr('x2', function(d) { return arc.centroid(d)[0] * 2.2; })
        .attr('y2', function(d) { return arc.centroid(d)[1] * 2.2; });

    //添加弧外的文字元素
    arcs.append('text')
        .attr('transform', function(d) {
            var x = arc.centroid(d)[0] * 2.5;
            var y = arc.centroid(d)[1] * 2.5;
            return 'translate(' + x + ',' + y + ')';
        })
        .attr('text-anchor', 'middle')
        .text(function(d) {
            return d.data[0];
        });
</script>
</body>
</html>
