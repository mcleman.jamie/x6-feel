function display() {
  var tweets = document.getElementsByClassName("tweet");
  for (index = 0; index < tweets.length; index++)
  {
    var id = tweets[index].getAttribute("tweetID");
    twttr.widgets.createTweet(
      id, tweets[index],
      {
        conversation : 'none',    // or all
        cards        : 'hidden',  // or visible
        linkColor    : '#cc0000', // default is blue
        theme        : 'light'    // or dark
      })
    .then (function (el) {
      el.contentDocument.querySelector(".footer").style.display = "none";
    });
  }
}

display();
