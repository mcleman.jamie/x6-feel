let percentage = [75, 10, 45];
let colors = [[76,217,100], [90,200,250], [255,59,48]]

function setup() {
    var canvas = createCanvas(300, 300);
    canvas.parent('graph-holder');
    noStroke();
    angleMode(DEGREES);
    noLoop(); // Run once and stop
}

function draw() {
    pieChart(300, percentage);
}

function pieChart(diameter, data) {
    let lastAngle = 0;
    for (let i = 0; i < data.length; i++) {
        fill(colors[i]);
        arc(
            width / 2,
            height / 2,
            diameter,
            diameter,
            lastAngle,
            lastAngle + (360 * percentage[i] / (percentage[0] + percentage[1] + percentage[2]))
        );
        lastAngle += (360 * percentage[i] / (percentage[0] + percentage[1] + percentage[2]));
    }
}