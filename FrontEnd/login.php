<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <!-- Bootstrap -->
	<link href="css/bootstrap-4.2.1.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet" type="text/css">
  </head>
  <body>
  	<!-- body code goes here -->
	<div class="container-fluid">
		<div class="row justify-content-md-center">
			<div class="col-10"></div>
			<div class="col">
			</div>
		</div>
		<div class="centered">
		  	<div class="col"></div>

				<div class="jubotron">
					<form action="includes/process_login.php" method="post" onsubmit="return beforeSubmit()">
					<div class="form-group">
						<div class="col">
							<div class="row">
								<input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Email" required>
								<input type="password" class="form-control" id="exampleInputPassword1" name="p" placeholder="Password" required>
							</div>
							<div class="row">
								<div class="btn-group btn-group-justified btn-block">
									<button type="submit" class="btn btn-success">Log In</button>
                                    <a href="register.php"><button type="button" class="btn btn-primary">Register</button></a>
								</div>
							</div>
						</div>
					</div>
				</form>
				</div>
				<!-- <div class="input-group mb-3 input-group-lg">
			  		<input type="text" class="form-control" placeholder="# Search..." aria-label="">
			  		<div class="input-group-append ">
			    		<button class="btn btn-primary" type="button">Search</button>
		    		</div>
		  		</div>
						<div id="holder">
						</div> -->
		</div>
	</div>
  	<!-- body code goes here -->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery-3.3.1.min.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap-4.2.1.js"></script>

    <script src="js/sha512.js"></script>
    <script>
        function beforeSubmit(){
            var ps = $("#exampleInputPassword1").val();
            $("#exampleInputPassword1").val(hex_sha512(ps));
//        console.log($("#password").val().length);
            return true;
        }
    </script>
  </body>
</html>
