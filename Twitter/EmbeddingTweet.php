<!DOCTYPE html>
<head>

</head>
<body>

  <style>

    #tweet {
      width: 400px !important;
    }

    #tweet iframe {
      border: none !important;
      box-shadow: none !important;
    }



  </style>

<?php
require_once("twitter-api-php/TwitterAPIExchange.php");
require_once("searchAPI_byUser_test.php");

function embed($arrayOfTweets)
{
  $result = "";
  //echo sizeOf($arrayOfTweets[0]) . "\n";
  for($index = 0; $index < sizeOf($arrayOfTweets[0]); $index++)
  {
    $tweetID = $arrayOfTweets[0][$index];
    //echo $index . "\n";
    //echo $tweetID . "\n";
    $result .= "<div id='tweet' tweetID=" . $tweetID . "></div>";
  }
  echo $result;
  return $result;
}

$tweets = searchAPI_byUser_test("randomString");
echo embed($tweets);


?>

  <!-- <div id="tweet" tweetID= ></div> -->

  <script sync src="https://platform.twitter.com/widgets.js"></script>

  <script>

    window.onload = (function(){

      var tweet = document.getElementById("tweet");
      var id = tweet.getAttribute("tweetID");

      twttr.widgets.createTweet(
        id, tweet,
        {
          conversation : 'none',    // or all
          cards        : 'hidden',  // or visible
          linkColor    : '#cc0000', // default is blue
          theme        : 'light'    // or dark
        })
      .then (function (el) {
        el.contentDocument.querySelector(".footer").style.display = "none";
      });

    });

  </script>


</body>
