import math
import random
import sys
from enum import Enum


class Classification(Enum):
    NEGATIVE = 0, 'Negative'
    NEUTRAL = 1, 'Neutral'
    POSITIVE = 2, 'Positive'

    def __new__(cls, value, name):
        member = object.__new__(cls)
        member._value_ = value
        member.fullname = name
        return member

    def __int__(self):
        return self.value


class Model:

    def __init__(self):
        self.__phraseCounts = [0, 0, 0]
        self.__wordCounts = [0, 0, 0]

    def incrementPhraseCount(self, classification):
        self.__phraseCounts[int(classification)] += 1

    def incrementWordCount(self, classification):
        self.__wordCounts[int(classification)] += 1

    def decrementWordCount(self, classification, amount):
        self.__wordCounts[int(classification)] -= amount

    def getPhraseCount(self, classification):
        return self.__phraseCounts[int(classification)]

    def getWordCount(self, classification):
        return self.__wordCounts[int(classification)]

    def getTotalPhraseCount(self):
        return sum(self.__phraseCounts)

    def wordCountBelowCutOff(self, word, percentage, uniqueWordCount):
        averageWordCount = sum(self.__wordCounts) / uniqueWordCount
        return word.getTotalCount() < averageWordCount * percentage


class Word:
    def __init__(self, word):
        self.__word = word
        self.__counts = [0, 0, 0]
        self.__logProbabilities = [0, 0, 0]

    def getWord(self):
        return self.__word

    def getCount(self, classification):
        return self.__counts[int(classification)]

    def getTotalCount(self):
        return sum(self.__counts)

    def getLogProbability(self, classification):
        return self.__logProbabilities[int(classification)]

    def calculateLogProbabilities(self, uniqueWordCount):
        for i in range(3):
            classification = Classification(i)
            self.__logProbabilities[i] = math.log((self.getCount(classification) + 1)
                                                  / (model.getWordCount(classification)
                                                     + uniqueWordCount))

    def incrementCount(self, classification):
        self.__counts[int(classification)] += 1


class Phrase:
    def __init__(self, classification, phrase):
        self.__phrase = phrase
        self.__classification = classification

    def getPhrase(self):
        return self.__phrase

    def getClassification(self):
        return self.__classification


# Method to remove punctuation from a given word
def removePunctuation(word):
    processedString = ""
    word = word.lower().strip()
    for char in word:
        if ord('a') <= ord(char) <= ord('z'):
            processedString += char
    return processedString


def getSortKey(item):
    return item[1].getTotalCount()


# Method returns the probability of a phrase occuring in a given class
# classIndex can take values:
# 0 - Negative
# 1 - Neutral
# 2 - Positive
def calculateProbabilityForClassAtIndex(classification, inputWords):
    probability = 0
    for index in range(0, len(inputWords)):
        word = removePunctuation(inputWords[index])
        if word not in stopWords:
            if word not in database.keys():
                probability += math.log(1 / (model.getWordCount(classification) + uniqueWordCount))
            else:
                probability += database[word].getLogProbability(classification)
    return probability


# List to store all phrases
allPhrases = []

# Read phrases and classifications from Dataset.txt file
with open("Dataset.txt", "r", encoding="utf-8") as trainingData:
    for line in trainingData.readlines():
        phrase = line.split("\t")
        classification = Classification(int(phrase[0]) // 2)
        if phrase[1].find("\n") != -1:
            allPhrases.append(Phrase(classification, phrase[1][:phrase[1].find("\n")]))
        else:
            allPhrases.append(Phrase(classification, phrase[1]))

totalNumberOfPhrases = len(allPhrases)

# List to store stopwords
stopWords = []

# Read stopwords from stopwords.txt file
with open("stopwords.txt", "r") as stopWordsFile:
    for line in stopWordsFile.readlines():
        if line.find("\n") != -1:
            stopWords.append(line[:line.find("\n")])
        else:
            stopWords.append(line)

# Variables to calculate total success rate
successCount = 0
totalSoFar = 0

# Repeat testing process amount of times specified by commandline argument
for i in range(0, int(sys.argv[1])):
    model = Model()
    print("Run " + str(i + 1))
    print("Splitting phrases...")

    # Lists to store this runs training and testing phrases respectively
    trainingPhrases = []
    testingPhrases = []

    # Randomly put half the phrases in testingPhrases and half in trainingPhrases
    for phrase in allPhrases:
        if (len(testingPhrases) == totalNumberOfPhrases / 2):
            trainingPhrases.append(phrase)
        elif (len(trainingPhrases) == totalNumberOfPhrases / 2):
            testingPhrases.append(phrase)
        elif (random.randint(0, 1) == 0):
            trainingPhrases.append(phrase)
        else:
            testingPhrases.append(phrase)

    # Database to store the machine learning model
    database = {}

    # Lists to store the phrase and word counts for each class where:
    # index 0 - Negative
    # index 1 - Neutral
    # index 2 - Positive
    uniqueWordCount = 0

    print("Training algorithm...")
    for phrase in trainingPhrases:
        classification = phrase.getClassification()
        model.incrementPhraseCount(classification)
        words = phrase.getPhrase().split(" ")

        for word in words:
            word = removePunctuation(word)
            if word not in stopWords:
                uniqueWordCount += 1
                # Add word to database if not already there
                if word not in database.keys():
                    database[word] = Word(word)
                database[word].incrementCount(classification)
                model.incrementWordCount(classification)

    uniqueWordCount = len(database)
    # Remove the least common words
    wordsToRemove = []
    sortedDatabase = sorted(database.items(), key=getSortKey, reverse=True)
    truncatedDatabase = {}
    desiredWordCount = int(sys.argv[2])
    count = 0
    for (key, value) in sortedDatabase:
        count += 1
        truncatedDatabase[key] = value
        if count >= desiredWordCount:
            break
    database = truncatedDatabase
    uniqueWordCount = len(database)
    # Update database to store ln(p(word|class)) using laplace smoothing
    for word in database.values():
        word.calculateLogProbabilities(uniqueWordCount)

    totalPhraseCount = model.getTotalPhraseCount()

    print("Testing algorithm...")
    for phrase in testingPhrases:
        # Calculate probability for each class
        probabilityPositive = calculateProbabilityForClassAtIndex(Classification.POSITIVE,
                                                                  phrase.getPhrase().split(" "))
        probabilityPositive += math.log(model.getPhraseCount(Classification.POSITIVE) / totalPhraseCount)
        probabilityNeutral = calculateProbabilityForClassAtIndex(Classification.NEUTRAL, phrase.getPhrase().split(" "))
        probabilityNeutral += math.log(model.getPhraseCount(Classification.NEUTRAL) / totalPhraseCount)
        probabilityNegative = calculateProbabilityForClassAtIndex(Classification.NEGATIVE,
                                                                  phrase.getPhrase().split(" "))
        probabilityNegative += math.log(model.getPhraseCount(Classification.NEGATIVE) / totalPhraseCount)
        # Get most likely classification of phrase
        if probabilityPositive == max(probabilityPositive, probabilityNeutral, probabilityNegative):
            classification = Classification.POSITIVE
        elif probabilityNeutral == max(probabilityPositive, probabilityNeutral, probabilityNegative):
            classification = Classification.NEUTRAL
        else:
            classification = Classification.NEGATIVE

        # Increment successCount if the classification was correct
        successCount += 1 if phrase.getClassification() == classification else 0
    totalSoFar += len(testingPhrases)
    print()

# Print success rate
print(str(round(successCount * 100 / totalSoFar, 3)) + "%")
