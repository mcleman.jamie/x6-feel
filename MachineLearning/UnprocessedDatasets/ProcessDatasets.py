from enum import Enum


class Classification(Enum):
    NEGATIVE = 0, 'Negative'
    NEUTRAL = 2, 'Neutral'
    POSITIVE = 4, 'Positive'

    def __new__(cls, value, name):
        member = object.__new__(cls)
        member._value_ = value
        member.fullname = name
        return member

    def __int__(self):
        return self.value


class Phrase:
    def __init__(self, phrase, classification):
        # Remove mentions, and urls from tweets
        while (phrase.find("RT @") != -1):
            index = phrase.find("RT @")
            space = (phrase.find(" ", index) + 1 if phrase.find(" ", index) != -1 else len(phrase))
            phrase = phrase[:index] + phrase[space:]
        while (phrase.find("@") != -1):
            index = phrase.find("@")
            space = (phrase.find(" ", index) + 1 if phrase.find(" ", index) != -1 else len(phrase))
            phrase = phrase[:index] + phrase[space:]
        while (phrase.find("http://") != -1):
            index = phrase.find("http://")
            space = (phrase.find(" ", index) + 1 if phrase.find(" ", index) != -1 else len(phrase))
            phrase = phrase[:index] + phrase[space:]
        while (phrase.find("https://") != -1):
            index = phrase.find("https://")
            space = (phrase.find(" ", index) + 1 if phrase.find(" ", index) != -1 else len(phrase))
            phrase = phrase[:index] + phrase[space:]
        self.__phrase = phrase
        self.__classification = classification

    def getPhrase(self):
        return self.__phrase

    def getClassification(self):
        return self.__classification


def readTweetsFromFile(file, phrases, classIndex, tweetIndex):
    for line in file.readlines():
        line = line.strip()
        index = -1

        # Skip first 5 fields
        for i in range(classIndex):
            index = line.find(",", index + 1)

        # Get tweets classification
        classification = line[index + 1: line.find(",", index + 1)].lower()
        if (classification == "neutral"):
            classification = Classification.NEUTRAL
        elif (classification == "positive"):
            classification = Classification.POSITIVE
        elif (classification == "negative"):
            classification = Classification.NEGATIVE

        # Skip next 10 fields
        for i in range(tweetIndex - classIndex):
            index = line.find(",", index + 1)

        # Get tweet and remove illigal characters
        tweet = line[index + 1: line.find(",", index + 1)]

        phrases.append(Phrase(tweet, classification))

    return phrases


phrases = []

with open("GOP_Debate.csv", "r", encoding="utf8") as trainingDataFile:
    phrases = readTweetsFromFile(trainingDataFile, phrases, 5, 15)

with open("US_Airlines.csv", "r", encoding="utf8") as trainingDataFile:
    readTweetsFromFile(trainingDataFile, phrases, 1, 10)

# Write phrases to new file
with open("TrainingData.txt", "w", encoding="utf8") as trainingDataFile:
    for phrase in phrases:
        trainingDataFile.write(str(int(phrase.getClassification())) + "\t" + phrase.getPhrase() + "\n")
