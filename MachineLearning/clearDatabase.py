import MySQLdb

db = MySQLdb.connect(host="dbhost.cs.man.ac.uk", user="f12695jm",
                     passwd="jmcleman", db="2018_comp10120_x6")
c = db.cursor()
c.execute("DELETE FROM NaiveBayesClassifierModel")
c.execute("DELETE FROM NaiveBayesModelPhraseCount")
db.commit()
c.close()
