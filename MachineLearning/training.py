import MySQLdb
from enum import IntEnum
from math import log

# Enum for the different classes of words
# positive, negative and neutral
class Classifier(IntEnum):
    POSITIVE = 0
    NEGATIVE = 1
    NEUTRAL = 2

# Class that stores a word and the probabilites for that word
class WordProb:
    word = "" # The word itself
    classifierCounts = [] # The number of times it occurs in each class
    classifierProbabilities = [] # The probabilites
    # Constructor
    def __init__(self, Word, ClassifierNum):
        self.word = Word
        self.classifierCounts = [1.0, 1.0, 1.0] # Starts at 1 for laplace smoothing
        self.classifierProbabilities = [0.0, 0.0, 0.0]
        self.incrementClassifer(ClassifierNum)
    # Increment one of the classifier counts
    def incrementClassifer(self, ClassifierType):
        self.classifierCounts[ClassifierType] += 1
    # Calculates the probabilites
    def calculateProbabilities(self, NumUniqueWords, NumClassifierWords):
        # P(Positive) = log(numTimesThisWordWasPositive / (TotalWords + TotalPositiveWords))
        self.classifierProbabilities[Classifier.POSITIVE] = log(self.classifierCounts[Classifier.POSITIVE] / (NumUniqueWords + NumClassifierWords[Classifier.POSITIVE]))
        # P(Negative) = log(numTimesThisWordWasNegative / (TotalWords + TotalNegativeWords))
        self.classifierProbabilities[Classifier.NEGATIVE] = log(self.classifierCounts[Classifier.NEGATIVE] / (NumUniqueWords + NumClassifierWords[Classifier.NEGATIVE]))
        # P(Neutral) = log(numTimesThisWordWasNeutral / (TotalWords + TotalNeutralWords))
        self.classifierProbabilities[Classifier.NEUTRAL] = log(self.classifierCounts[Classifier.NEUTRAL] / (NumUniqueWords + NumClassifierWords[Classifier.NEUTRAL]))

# This class is responsible for interfacing with the database
class Database:
    # Constructor
    def __init__(self):
        self.db = 0
        self.c = 0
    # Open the databse
    def open(self):
        # Connect using MySQLdb
        self.db = MySQLdb.connect(host="dbhost.cs.man.ac.uk", user="f12695jm",
                                  passwd="jmcleman", db="2018_comp10120_x6")
        # Create a cursor
        self.c = self.db.cursor()
    # Close the database
    def close(self):
        self.db.commit() # Commit changes
        self.c.close() # Close
    # Write a word
    def writeWord(self, Word):
        # This creates a string containing the SQL
        sqlString = "INSERT INTO NaiveBayesClassifierModel (UID, Word, Positive, Negative, Neutral)"
        sqlString += " VALUES (NULL, '" + Word.word + "', '" + str(Word.classifierProbabilities[Classifier.POSITIVE]) + "', '"
        sqlString += str(Word.classifierProbabilities[Classifier.NEGATIVE]) + "', '" + str(Word.classifierProbabilities[Classifier.NEUTRAL]) + "')"
        self.c.execute(sqlString)

    # Writes to NaiveBayesModelPhraseCount table
    def writeToPhraseCount(self, ClassProbability, UnknownWordProbability):
        # Writes data for each class 0, to 2
        for i in range(0, 3):
            sqlString = "INSERT INTO NaiveBayesModelPhraseCount (UID, Class, ClassProbability, UnknownWordProbability)"
            sqlString += " VALUES (NULL, '" + str(i) + "', '" + str(ClassProbability[i]) + "', '"
            sqlString += str(UnknownWordProbability[i]) + "')"
            self.c.execute(sqlString)

# This class is responsible for reading the data from the dataset
class Training:
    # Constructor
    def __init__(self):
        self.database = Database()
        self.words = []
        self.stopWords = []
        self.numClassifierWords = [0.0, 0.0, 0.0]
        self.numUniqueWords = 0
        self.numClassifierTweets = [0.0, 0.0, 0.0]
        self.numTweets = 0.0;
    # Run
    def run(self):
        self.readStopWords()
        self.readDataset()
        #self.outputWords()
        print("Writing to database...")
        self.writeToDatabase()

    def readStopWords(self):
        with open("stopwords.txt", "r") as datasetFile:
            data = datasetFile.readlines()
            for lineIndex, nextLine in enumerate(data, 0):
                self.stopWords.append(nextLine.rstrip("\n"))

    # Reads the dataset
    def readDataset(self):
        # Open Dataset.txt
        with open("Dataset.txt", "r") as datasetFile:
            data = datasetFile.readlines() # Read the lines
            # Iterates through each line where the line is stored in nextLine
            for lineIndex, nextLine in enumerate(data, 0):
                classifierNum = self.mapClassifier(int(nextLine[0])) # Get the classifierNum which is the first char
                self.numTweets += 1 # Increment number of tweets
                self.numClassifierTweets[classifierNum] += 1 # Increment number of tweets for this class
                characterIndex = 2 # Sets the index to start of tweet
                # Reset these strings
                nextWord = ""
                nextCharacter = ""
                # Iterates through each character of the current line
                while characterIndex < len(nextLine):
                    nextCharacter = nextLine[characterIndex] # Get the next character
                    # Check if the character is not a letter
                    if self.checkAsciiCode(nextCharacter) == False:
                        # If the next character is an apostrophe increment the character index
                        if nextCharacter == "'":
                            characterIndex += 1
                            continue
                        # If the nextWord is not empty
                        elif nextWord != "":
                            self.addWord(nextWord, classifierNum) # Add this word to database
                            nextWord = "" # Reset nextWord
                        # If word empty and next character is not apostrophe
                        else:
                            characterIndex += 1 # Increment character index
                    # The next character is a letter
                    else:
                        # Add character to the word
                        nextWord = nextWord + nextCharacter
                        characterIndex += 1 # Increment character index
                if lineIndex % 500 == 0:
                    print("line: " + str(lineIndex))

    # This maps the classifier numbers used in Dataset.txt to the numbers
    # used in training.py.
    # 0 -> 1     2 -> 2      4 -> 0
    def mapClassifier(self, InputClassifier):
        if InputClassifier == 0:
            return 1
        elif InputClassifier == 2:
            return 2
        elif InputClassifier == 4:
            return 0
        return 0

    # Checks the ascii code of a character and returns true if its from
    # a-z or A-Z. Otherwise it returns false
    def checkAsciiCode(self, Character):
        asciiCode = ord(Character) # Get the ascii code
        # A-Z
        if asciiCode > 64 and asciiCode < 91:
            return True
        # a-z
        elif asciiCode > 96 and asciiCode < 123:
            return True
        return False

    # Checks if a word is a stop word
    # True if it is, false if it isn't
    def isStopWord(self, Word):
        # If the word is in stop words list
        if Word in self.stopWords:
            return True # It is a stopword
        return False # Else, it isn't a stopword

    # Adds a word to the wordProbs list or increments if it's already there
    def addWord(self, Word, ClassifierNum):
        Word = Word.lower() # Make the word lowercase
        # Check if it's a stopword
        if self.isStopWord(Word) == True:
            return # If it is don't add the word
        self.numClassifierWords[ClassifierNum] += 1
        for item in self.words:
            if item.word == Word:
                item.incrementClassifer(ClassifierNum)
                return
        self.numUniqueWords += 1
        self.words.append(WordProb(Word, ClassifierNum))

    # This outputs all words in self.words with the probabilities
    # Used in testing
    def outputWords(self):
        for item in self.words:
            print(item.word + " " + str(item.classifierCounts[0]) + str(item.classifierCounts[1]) + str(item.classifierCounts[2]))

    # Writes the words and probabilities to the database
    def writeToDatabase(self):
        self.database.open() # Open the database
        # Iterate through each item in self.words
        for item in self.words:
            # Calculate the probabilities
            item.calculateProbabilities(self.numUniqueWords, self.numClassifierWords)
            self.database.writeWord(item) # Write to database

        # Calculate the classProbability and the unknownWordProbability
        classProbability = [0.0 ,0.0, 0.0]
        unknownWordProbability = [0.0, 0.0, 0.0]
        for i in range(0, 3):
            classProbability[i] = log(self.numClassifierTweets[i] / self.numTweets)
            unknownWordProbability[i] = log(1.0 / (self.numUniqueWords + self.numClassifierWords[i]))

        # Write this data just calculated
        self.database.writeToPhraseCount(classProbability, unknownWordProbability)
        self.database.close() # Close the database

training = Training() # Create training object
training.run() # Run

