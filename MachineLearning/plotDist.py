import matplotlib.pyplot as plt
from training import Training
import math

model = Training() # Create training object
model.run("Dataset.txt") # Run

words = []
x = []
y = []
positive = []
neutral = []
negative = []

for word in model.words:
  word.calculateProbabilities(model.numUniqueWords, model.numClassifierWords)
  words.append(word)
  
words = sorted(words, key=lambda word: sum(word.classifierProbabilities))
positive = sorted(words, key=lambda word: word.classifierProbabilities[2])
neutral = sorted(words, key=lambda word: word.classifierProbabilities[1])
negative = sorted(words, key=lambda word: word.classifierProbabilities[0])

positiveY = []
neutralY = []
negativeY = []

for word in positive:
  positiveY.append(word.classifierProbabilities[2])
  
for word in neutral:
  neutralY.append(word.classifierProbabilities[1])

for word in negative:
  negativeY.append(word.classifierProbabilities[0])

count = 0
print(len(words))
for word in words:
  count += 1
  x.append(count)
  y.append(sum(word.classifierProbabilities))

plt.figure(1)

plt.subplot(121)
plt.plot(x, y)
plt.subplot(122)
plt.plot(x, negativeY, x, neutralY, x, positiveY)
plt.suptitle('Log probabilities distributions')
plt.show()
